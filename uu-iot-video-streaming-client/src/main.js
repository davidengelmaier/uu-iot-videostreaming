'use strict';
const
    WebSocket = require('websocket'),
    fs = require('fs')
;

const WebSocketClient = WebSocket.client;
const client = new WebSocketClient();
let MIN_FRAME_LENGTH = 800;
let shouldStream = false;
let serverConnection = null;

client.on('connectFailed', (error) => {
    console.log('Connect Error: ' + error.toString());
    setTimeout(connectToServer, 1000);
});

client.on('connect', (connection) => {
    serverConnection = connection;
    console.log('WebSocket Client Connected');
    connection.on('error', (error) => {
        console.log("Connection Error: " + error.toString());
    });
    connection.on('close', () => {
        console.log('echo-protocol Connection Closed');
        connectToServer();
    });
    connection.on('message', (message) => {
        if (message.type === 'utf8') {
            console.log("Received: '" + message.utf8Data + "'");
            if (message.utf8Data === 'start') {
                shouldStream = true;
            } else if (message.utf8Data === 'stop') {
                shouldStream = false;
            } else {
                MIN_FRAME_LENGTH = message.utf8Data;
            }
        console.log(shouldStream);
        }
    });
});

fs.watch('/home/pi/FTP', (eventType, filename) => {
    if (shouldStream && filename && eventType === 'change' && serverConnection) {
        fs.readFile(`/home/pi/FTP/${filename}`, (error, data) => {
           if (error) {
               console.log(error);
           } else if (data.buffer.byteLength > MIN_FRAME_LENGTH) {
               serverConnection.sendBytes(data, () => {});
           }
        });
    }
});

const connectToServer = () => {
    console.log('connecting to server');
    client.connect('ws://uu-iot-video-streaming.herokuapp.com', 'pi-upload');
};

connectToServer();
