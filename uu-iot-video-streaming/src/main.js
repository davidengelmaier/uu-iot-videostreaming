'use strict';
const
    WebSocket = require('websocket'),
    http = require('http'),
    fs = require('fs'),
    express = require('express'),
    path = require('path')
;

const app = express();
const PORT = process.env.PORT || 3000;
let MIN_FRAME_LENGTH = 800;
let piConnection = null;
let response;
let responses = [];
let clients = [];
let streaming = false;

app.use(express.static(__dirname + '/public'));

app.get('/start', (req, res) => {
    if (!streaming && piConnection) {
        streaming = !streaming;
        piConnection.sendUTF('start');
    }
    res.status(200).end();
});

app.get('/stop', (req, res) => {
    if (streaming && clients.length === 1 && piConnection) {
        streaming = !streaming;
        piConnection.sendUTF('stop');
    }
    res.status(200).end();
});

app.get('/stream', (req, res) => {
    response = res;
    responses.push(res);

    res.writeHead(200, {
        'Content-Type': 'multipart/x-mixed-replace; boundary=myboundary',
        'Cache-Control': 'no-cache',
        'Connection': 'close',
        'Pragma': 'no-cache'
    });
});

const sendFrame = () => {
    responses.length;
    fs.readFile(path.join(__dirname+'/images/sent-image.jpg'), (err, data) => {
        if (!response || err || data == undefined || data.length < MIN_FRAME_LENGTH) {
            if (!response) {
                piConnection.sendUTF('stop');
            }
            return;
        }
        responses.forEach((element) => {
            element.write("--myboundary\r\n");
            element.write("Content-Type: image/jpeg\r\n");
            element.write("Content-Length: " + data.length + "\r\n");
            element.write("\r\n");
            element.write(data, 'binary');
            element.write("\r\n");
            console.log('frame sent:' + data.length);
        });
    });
};

app.httpServer = http.createServer(app);
app.httpServer.listen(PORT, () => {
    console.log(`${new Date()} Server is listening on port ${PORT}`);
});

const WebSocketServer = WebSocket.server;
const wsServer = new WebSocketServer(
    {
        httpServer: app.httpServer,
        autoAcceptConnections: false
    }
);

wsServer.on('request', (request) => {
    if (request.protocolFullCaseMap.hasOwnProperty('pi-upload')) {
        piConnection = request.accept('pi-upload', request.origin);
        console.log((new Date()) + ' Connection accepted.');
        piConnection.on('message', (message) => {
            if (message.type === 'utf8') {
                console.log('Received Message: ' + message.utf8Data);
                piConnection.sendUTF(message.utf8Data);
            }
            else if (message.type === 'binary' && message.binaryData.length > MIN_FRAME_LENGTH) {
                // console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
                fs.writeFile(path.join(__dirname + '/images/sent-image.jpg'), message.binaryData, (error) => {
                    if (error) throw error;
                    console.log('File saved!');
                    sendFrame();
                })
            }
        });
        piConnection.on('close', (reasonCode, description) => {
            console.log((new Date()) + ' Peer ' + piConnection.remoteAddress + ' reasonCode: ' + reasonCode + ' disconnected.');
        });
    } else if (request.protocolFullCaseMap.hasOwnProperty('client-download')) {
        let newClient = request.accept('client-download', request.origin);
        newClient.on('message', (message) => {
            MIN_FRAME_LENGTH = parseInt(message.utf8Data);
            piConnection.sendUTF(MIN_FRAME_LENGTH);
        });
        newClient.on('close', (reasonCode, description) => {
            clients.pop(newClient);
            if (streaming && clients.length === 0 && piConnection) {
                streaming = !streaming;
                piConnection.sendUTF('stop');
            }
        });
        newClient.send(MIN_FRAME_LENGTH);
        piConnection.sendUTF(MIN_FRAME_LENGTH);
        clients.push(newClient);
    }
});
