This project is a solution to using the combination of a cheap ip cam, raspberry pi and cloud based logic to observe
images from the cam without the need of a static IP address at the point of the cam connection.

##### Test current installation
1. go to http://uu-iot-video-streaming.herokuapp.com/
2. click `Start Streaming`
3. if you do not see any image, try to lower the `Minimum frame size in bytes` value
4. if you see the image flicker too much, try to augment the `Minimum frame size in bytes` value

##### Cloud
1. deploy uu-iot-streaming to your selected cloud environment

##### Raspberry Pi setup
1. download prepared iso image from https://uloz.to/!6YYeBbyN5RXN/pi-iso and flash it to an SD card
2. boot from the card, login as pi:raspberry
3. setup the IP address in your network
4. reboot
5. goto ~/uu-iot-video-streaming-client
6. edit `client.connect()` method on line #57 to match the URL of your cloud deployment 
7. start the client by `npm start &`

##### Camera setup
1. connect the cam to the network by following your cam's manufacturer's instructions
2. setup the video mode at http://YOUR_CAM_IP/video.htm to:
Resolution: 320x240
FPS: Auto
Jpeg Quality: Medium
View Mode: Image
3. setup the FTP with the following settings:
IP address: your Pi
Port: 21
usernme: upload
password: upload
Path: /
Passive Mode: Yes
Enable uploading images to an FTP server: check
Always: check
Image Frequency: Auto
Sequence Number Suffix Up to: 3

##### Implementation
1. Raspberry Pi:
Raspian based image containing node.js runtime and ftp server
Node.js Javascript based client logic(`uu-iot-video-streaming-client`) readsthe images uploaded by the cam to the ftp server and sends them through a websocket connection to the cloud logic

2. Cloud logic:
Server side Node.js Javascript implementation, which opens a websocket connection for the Pi client to connect to and handles the relay between the Pi and the client browser logic. It communicates
with the Pi client to let it know if there are any browser clients interested in the stream, in case nobody's watching, then no stream is pushed to the cloud from Pi.

3. Browser client:
Opens a websocket connection to the cloud to serve as a `presence` channel, so the cloud logic can count the number of clients being connected and stop/start streaming only for the last/first
client. Opens a http connection with multipart multipart/x-mixed-replace content type method, this connection is used by the client to display the image and by the cloud to push the image data coming from Pi.
